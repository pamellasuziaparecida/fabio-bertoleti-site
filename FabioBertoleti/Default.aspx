﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Clínica odontológica</title>
    <%-- CSS --%>
    <link type="text/css" rel="stylesheet"  href="Content/bootstrap.min.css" />
    <link href="_css/reset.css" rel="stylesheet" />
    <link href="_css/global.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        
        <%-- Header --%>

        <header class="cabecalho" id="cabecalho">

            <div class="container">

                <div class="row hdr-line">

                    <div class="col-md-6 hdr-col-logo">
                        <img src="_img/logo_color.png" />
                    </div>

                    <div class="col-md-6 flex-sup hdr-col-nav">
                        <nav class="flex-sup">
                            <a href="#"> Serviços </a>
                            <a href="#"> Contato </a>
                            <a href="#"> Sobre nós </a>
                        </nav>
                        <a class="flex-sup hdr-cta"> Agendar consulta </a>
                    </div>

                </div>

            </div>

            <figure id="hdr-vector"> <img src="_svg/header_logo_path.svg" /> </figure>

        </header>

        <%-- Hero --%>

        <section class="site_sacao" id="secao_hero">

            <div class="flex-sup container">

                <h1> Clínica <br /> odontológica </h1>

                <h2>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras suscipit elit sed massa lacinia, et efficitur tellus ultricies. 
                </h2>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras suscipit elit sed massa lacinia, et efficitur tellus ultricies. Curabitur vestibulum, augue pretium luctus dignissim.
                    </p>

                <div class="flex-sup heroCtaLine">
                    <a class="flex-sup hero_cta" href="#"> <small> Cta principal </small> </a>
                    <a class="flex-sup hero_cta hroCta_secundario" href="#"> <small> Cta secundário </small> </a>
                </div>

            </div>

            <figure class="flex-sup" id="hero_img"> <img src="_img/hq_herobg_redux.png" /> </figure>

        </section>

        <%-- Attract mode --%>

        <section class="site_sacao" id="secao_especialis">
            
            <div class="flex-sup container">

                <div class="flex-sup section-hgroup">
                    <h2> Cuidamos de você, </h2>
                    <h3> em qualquer situação. </h3>
                </div>

                <ul class="flex-sup lista-special">

                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9358.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20275.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp1.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9360.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20276.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp2.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9362.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20277.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp3.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9363.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20278.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp4.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9366.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20279.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp5.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9368.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20280.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp6.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9369.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20281.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp7.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9371.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20282.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp8.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>
                    
                    <%-- special card --%>
                    <li class="flex-sup">
                        <figure class="flex-sup splCrd-img" style="background: url('_img/fotos/IMG_9362.jpg')"> ~background </figure>
                        <div class="flex-sup spl-icon"> <img src="_img/icons/Group%20283.png" /> </div>
                        <figure class="flex-sup splCrd-svg"> <img src="_svg/cardpaths/cp9.svg" /> </figure>
                        <div class="flex-sup spcBoxHint"> <strong> Remoção do ciso </strong> <small> Isso faz isso </small> </div>
                    </li>

                </ul>

            </div>

        </section>

        <%-- Form principal --%>

       <section class="site_sacao" id="secao_form">
            
            <div class="flex-sup container">

                <h2> Agende sua visita agora </h2>

                <div class="row">
                        
                    <%-- < - explicação formulário --%>
                    <div class="col-md-6 col-form-text">

                        <h3> É fácil, rápido e sem erro, </h3>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent non tristique enim. Suspendisse ornare, lorem eget commodo dignissim, purus nibh iaculis odio, eget pharetra orci eros at ligula. Aenean vel pharetra felis. Donec purus quam, efficitur vel purus at, pretium faucibus neque. Mauris vulputate lobortis fringilla.
                            </p>

                        <figure class="flex-sup form-img">
                            <img src="_img/dentesBrilho.png" />
                        </figure>
                    </div>

                    <%-- > - Formulário --%>
                    <div class="flex-sup col-md-6 form-col">

                        <div class="form-card">

                            <%-- nome --%>
                            <div class="flex-sup ipt-box">
                                <label> Seu nome: </label>
                                <input class="dft-ipt" type="text" name="Contato Nome" />
                            </div>
                            
                            <%-- email --%>
                            <div class="flex-sup ipt-box">
                                <label> E-mail de contato: </label>
                                <input class="dft-ipt" type="email" name="Contato Email" />
                            </div>
                            
                            <%-- zap --%>
                            <div class="flex-sup ipt-box">
                                <label> Seu número (Whatsapp): </label>
                                <input class="dft-ipt" type="text" name="Contato Whatsapp" />
                            </div>

                            <label class="flex-sup acppt-check"> 
                                <input type="checkbox" name="aceitar_termos" class="dft-checkBox" name="aceitar termos" /> 
                                <small> Estou de acordo com os <strong> termos </strong> </small>
                            </label>

                            <a class="flex-sup hero_cta form-cta" href="#"> <small> Cadastro gratuito </small> </a>

                            <figure class="flex-sup zapIcon"> <img src="_img/zapIcon.png" /> </figure>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <%-- Features --%>

        <section class="flex-sup mini-section" id="odo-features">

            <div class="flex-sup container">

                <ul class="flex-sup odoFeaturesList">

                    <li class="flex-sup">
                        <figure class="flex-sup ftrImgWpp">
                            <img src="_img/features/Group%20285.png" />
                        </figure>
                        <strong> Agendamento online <br /> prático e inteligente </strong>
                    </li>

                    
                    <li class="flex-sup">
                        <figure class="flex-sup ftrImgWpp">
                            <img src="_img/features/Group%20286.png" />
                        </figure>
                        <strong> Profissionais de <br /> excelência </strong>
                    </li>
                    
                    <li class="flex-sup">
                        <figure class="flex-sup ftrImgWpp">
                            <img src="_img/features/Group%20287.png" />
                        </figure>
                        <strong> Atendimento de <br /> qualidade </strong>
                    </li>
                    
                    <li class="flex-sup">
                        <figure class="flex-sup ftrImgWpp">
                            <img src="_img/features/Group%20288.png" />
                        </figure>
                        <strong> Instalações premium <br /> de primeira linha </strong>
                    </li>
                    
                    <li class="flex-sup">
                        <figure class="flex-sup ftrImgWpp">
                            <img src="_img/features/Group%20289.png" />
                        </figure>
                        <strong> Planos que <br /> cabem no seu bolso </strong>
                    </li>
                    

                </ul>

            </div>

        </section>

        <%-- Localidade --%>

        <section class="site_sacao" id="secao_loc">
            
            <div class="flex-sup container">

                <div class="row">

                    <%-- < --%>
                    <div class="col-md-6 flex-sup siteMapaIframe">

                        <h2> Onde nos encontrar </h2>
                        <strong> Estamos disponíveis em </strong>
                        <p> R. Padre Geraldo Rodrigues de Oliveira, 14 - Centro, <br /> Guaratinguetá - SP, 12505-140 </p>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d919.4281406446444!2d-45.19722797075851!3d-22.813112262870064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ccc459fbb3b201%3A0x9426e2106d8c65a5!2sR.%20Padre%20Geraldo%20Rodrigues%20de%20Oliveira%2C%2014%20-%20Centro%2C%20Guaratinguet%C3%A1%20-%20SP%2C%2012505-140!5e0!3m2!1spt-BR!2sbr!4v1655725660960!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

                    </div>

                    <%-- > --%>
                    <div class="col-md-6 flex-sup agendaCol">

                        <div class="flex-sup agenda-card">

                            <h3> Estaremos disponíveis em: </h3>

                            <ul class="flex-sup">

                                <li class="flex-sup"> 
                                    <span class="icon-circle">o</span> <strong> Segunda-feira: </strong> <p> <b> 23/05/2022 </b> das 16:15 até as 21:45 </p> <a href="#"> agendar </a> 
                                </li>

                                <li class="flex-sup"> 
                                    <span class="icon-circle">o</span> <strong> Terça-feira: </strong> <p> <b> 23/05/2022 </b> das 16:15 até as 21:45 </p> <a href="#"> agendar </a> 
                                </li>

                                <li class="flex-sup"> 
                                    <span class="icon-circle">o</span> <strong> Quarta-feira: </strong> <p> <b> 23/05/2022 </b> das 16:15 até as 21:45 </p> <a href="#"> agendar </a> 
                                </li>

                                <li class="flex-sup"> 
                                    <span class="icon-circle">o</span> <strong> Quinta-feira: </strong> <p> <b> 23/05/2022 </b> das 16:15 até as 21:45 </p> <a href="#"> agendar </a> 
                                </li>

                                <li class="flex-sup"> 
                                    <span class="icon-circle">o</span> <strong> Sexta-feira: </strong> <p> <b> 23/05/2022 </b> das 16:15 até as 21:45 </p> <a href="#"> agendar </a> 
                                </li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>

        </section>

        <%-- Profissionais --%>

        <section class="site_sacao" id="secao_prof">
            
            <div class="flex-sup container">

                <div class="row">

                    <%-- < --%>
                    <div class="col-md-6 prof-explain">

                        <h2> Profissionais de qualidade </h2>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras suscipit elit sed massa lacinia, et efficitur tellus ultricies. Curabitur vestibulum, augue pretium luctus dignissim.
                        </p>

                        <a class="flex-sup hero_cta" href="#"> <small> Agende uma consulta já </small> </a>

                    </div>

                    <%-- > --%>
                    <div class="col-md-6 flex-sup profi-col">

                        <div class="flex-sup agenda-card profi-card">

                            <figure class="bgcenter profi-thumb" style="background: url('_img/fotos/profithumb.jpg')">> profi Bg </figure>

                            <strong> Dr. Fabio Bertoleti </strong>

                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras suscipit elit sed massa lacinia, et efficitur tellus ultricies. Curabitur vestibulum, augue pretium luctus dignissim.
                            </p>
                        </div>

                    </div>

                <//div>

            </div>

        </section>

        <%-- Prova social --%>

        <%-- Formulário secundário --%>

        <%-- Footer --%>

    </form>
</body>
</html>
